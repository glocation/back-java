CREATE DATABASE  IF NOT EXISTS `findmyglober` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `findmyglober`;
-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: findmyglober
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assignment`
--

DROP TABLE IF EXISTS `assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignment` (
  `idAssignment` int(11) NOT NULL AUTO_INCREMENT,
  `idProject` int(11) NOT NULL,
  `idGlober` int(11) NOT NULL,
  PRIMARY KEY (`idAssignment`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment`
--

LOCK TABLES `assignment` WRITE;
/*!40000 ALTER TABLE `assignment` DISABLE KEYS */;
INSERT INTO `assignment` VALUES (1,1,1),(2,52,1),(3,69,2);
/*!40000 ALTER TABLE `assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `idClient` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id for Client',
  `name` varchar(50) NOT NULL COMMENT 'Name of the Client',
  `description` varchar(100) DEFAULT NULL COMMENT 'Description of the Client',
  PRIMARY KEY (`idClient`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'LATAM','Latam Airline'),(2,'OPIUM','Oportum Credits');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glober`
--

DROP TABLE IF EXISTS `glober`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glober` (
  `idGlober` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id of the glober for this project',
  `userName` varchar(50) NOT NULL COMMENT 'UserName',
  `name` varchar(50) NOT NULL COMMENT 'Name',
  `lastName` varchar(50) NOT NULL COMMENT 'Last Name of the globber',
  `idSeat` int(11) NOT NULL COMMENT 'Id Seat on Catalog of Seats',
  `idRole` int(11) NOT NULL COMMENT 'User Rol for this application',
  `idProject` int(11) DEFAULT NULL COMMENT 'Path for picture',
  `phone` varchar(50) NOT NULL COMMENT 'Phone Number',
  `extension` varchar(50) NOT NULL COMMENT 'Extension of Phone Number',
  `mobile` varchar(50) NOT NULL COMMENT 'Mobile Phone Number',
  `mail` varchar(50) NOT NULL COMMENT 'Mail Address',
  PRIMARY KEY (`idGlober`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glober`
--

LOCK TABLES `glober` WRITE;
/*!40000 ALTER TABLE `glober` DISABLE KEYS */;
INSERT INTO `glober` VALUES (1,'j.morales','Javier','Morales',1,1,1,'5555555555','21087','5543548437','j.morales@globant.com'),(2,'oscar.ladino','Oscar','Ladino',2,2,69,'5555555555','21069','5543548666','oscar.ladino@globant.com');
/*!40000 ALTER TABLE `glober` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `idProject` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id of the project',
  `idClient` int(11) NOT NULL,
  `name` varchar(100) NOT NULL COMMENT 'Name of the project',
  `description` varchar(50) NOT NULL COMMENT 'Description of the project',
  PRIMARY KEY (`idProject`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1,1,'turpis egestas. Aliquam','2014-05-22'),(2,1,'vitae, sodales at,','2005-06-02'),(3,1,'vitae erat vel','2007-01-15'),(4,1,'pede nec ante','2009-05-29'),(5,1,'purus. Maecenas libero','2012-01-14'),(6,1,'id, blandit at,','2005-09-20'),(7,1,'amet diam eu','2011-07-25'),(8,1,'Fusce fermentum fermentum','2011-12-25'),(9,1,'lectus pede, ultrices','2016-07-11'),(10,1,'massa. Suspendisse eleifend.','2008-05-27'),(11,1,'metus facilisis lorem','2010-02-07'),(12,1,'amet, consectetuer adipiscing','2007-09-16'),(13,1,'nec urna et','2014-03-05'),(14,1,'nec, leo. Morbi','2005-04-02'),(15,1,'nec, mollis vitae,','2012-07-22'),(16,1,'ac, fermentum vel,','2009-01-31'),(17,1,'consectetuer, cursus et,','2016-04-24'),(18,1,'est mauris, rhoncus','2007-06-18'),(19,1,'aliquam eu, accumsan','2013-11-06'),(20,1,'magna. Nam ligula','2016-03-01'),(21,1,'pede, nonummy ut,','2007-11-03'),(22,1,'cursus non, egestas','2014-11-12'),(23,1,'enim. Curabitur massa.','2015-12-19'),(24,1,'sed pede. Cum','2008-05-31'),(25,1,'ridiculus mus. Aenean','2012-07-28'),(26,1,'nec urna et','2005-12-23'),(27,1,'ante dictum mi,','2016-01-25'),(28,1,'et, eros. Proin','2015-09-23'),(29,1,'sed pede. Cum','2012-04-12'),(30,1,'turpis egestas. Fusce','2016-01-01'),(31,1,'arcu. Sed et','2006-07-24'),(32,1,'odio. Phasellus at','2008-11-04'),(33,1,'nulla magna, malesuada','2014-04-21'),(34,1,'malesuada id, erat.','2009-04-06'),(35,1,'Pellentesque ultricies dignissim','2009-01-19'),(36,1,'et netus et','2015-01-08'),(37,1,'dictum mi, ac','2011-06-29'),(38,1,'lectus quis massa.','2012-03-06'),(39,1,'eget massa. Suspendisse','2013-07-03'),(40,1,'gravida non, sollicitudin','2007-10-28'),(41,1,'metus. Aliquam erat','2013-04-23'),(42,1,'aliquet molestie tellus.','2010-06-28'),(43,1,'Integer eu lacus.','2012-12-18'),(44,1,'tempor augue ac','2012-03-14'),(45,1,'ac urna. Ut','2015-12-19'),(46,1,'porttitor interdum. Sed','2009-01-04'),(47,1,'sit amet lorem','2013-08-18'),(48,1,'non, cursus non,','2009-04-23'),(49,1,'Integer sem elit,','2016-07-02'),(50,1,'nisi magna sed','2010-05-27'),(51,2,'Suspendisse aliquet molestie','2013-02-15'),(52,2,'lacinia orci, consectetuer','2014-02-26'),(53,2,'Duis dignissim tempor','2009-05-25'),(54,2,'purus gravida sagittis.','2015-06-15'),(55,2,'enim mi tempor','2014-04-18'),(56,2,'Integer vitae nibh.','2016-05-26'),(57,2,'vitae, erat. Vivamus','2009-02-10'),(58,2,'placerat, augue. Sed','2015-07-04'),(59,2,'ante blandit viverra.','2006-05-12'),(60,2,'neque. Nullam ut','2006-06-03'),(61,2,'dapibus gravida. Aliquam','2011-06-18'),(62,2,'Maecenas iaculis aliquet','2015-12-20'),(63,2,'felis. Donec tempor,','2009-12-29'),(64,2,'dui nec urna','2007-03-03'),(65,2,'egestas a, scelerisque','2013-12-25'),(66,2,'Mauris eu turpis.','2013-03-16'),(67,2,'Morbi metus. Vivamus','2012-07-02'),(68,2,'Cum sociis natoque','2016-11-19'),(69,2,'elementum at, egestas','2010-11-29'),(70,2,'ornare, lectus ante','2010-12-09'),(71,2,'tristique senectus et','2016-06-02'),(72,2,'Donec tempor, est','2008-07-18'),(73,2,'leo. Morbi neque','2006-07-06'),(74,2,'egestas blandit. Nam','2017-02-03'),(75,2,'mauris sit amet','2012-12-30'),(76,2,'vulputate ullamcorper magna.','2010-04-01'),(77,2,'lacus. Quisque imperdiet,','2016-12-25'),(78,2,'Mauris vestibulum, neque','2015-11-20'),(79,2,'sit amet, consectetuer','2014-04-22'),(80,2,'Aenean euismod mauris','2012-02-04'),(81,2,'dapibus ligula. Aliquam','2007-11-24'),(82,2,'mauris. Morbi non','2014-10-30'),(83,2,'orci lacus vestibulum','2008-10-22'),(84,2,'venenatis a, magna.','2007-04-27'),(85,2,'sem eget massa.','2007-08-15'),(86,2,'facilisis vitae, orci.','2010-07-07'),(87,2,'orci quis lectus.','2013-02-02'),(88,2,'laoreet, libero et','2016-01-06'),(89,2,'Sed pharetra, felis','2008-03-07'),(90,2,'turpis nec mauris','2010-10-26'),(91,2,'ligula. Donec luctus','2005-04-16'),(92,2,'sit amet orci.','2008-02-08'),(93,2,'lorem ut aliquam','2008-02-03'),(94,2,'a odio semper','2012-12-02'),(95,2,'et malesuada fames','2006-01-18'),(96,2,'Phasellus elit pede,','2009-08-01'),(97,2,'Phasellus dapibus quam','2016-04-11'),(98,2,'magna. Duis dignissim','2009-11-16'),(99,2,'eu nulla at','2015-12-05'),(100,2,'ut mi. Duis','2015-04-05');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `idRole` int(11) NOT NULL COMMENT 'Id of the Role',
  `name` varchar(50) NOT NULL COMMENT 'Type of Role',
  `description` varchar(256) NOT NULL COMMENT 'Role Description',
  PRIMARY KEY (`idRole`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Java Developer SR','Java Developer Senior'),(2,'Software Designer','Java Software Designer');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seat`
--

DROP TABLE IF EXISTS `seat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seat` (
  `idSeat` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id of the seat',
  `description` varchar(100) NOT NULL COMMENT 'Position of the seat place',
  `security` varchar(1) NOT NULL COMMENT 'Type of security for the seat',
  KEY `Índice 1` (`idSeat`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seat`
--

LOCK TABLES `seat` WRITE;
/*!40000 ALTER TABLE `seat` DISABLE KEYS */;
INSERT INTO `seat` VALUES (1,'MX-MX-16-I1A1','0'),(2,'MX-MX-15-I1A2','1');
/*!40000 ALTER TABLE `seat` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-02 13:58:59

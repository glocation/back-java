package com.globant.findmyglober.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.globant.findmyglober.dao.GloberDao;
import com.globant.findmyglober.entity.Glober;


@RestController
@RequestMapping("/glober")
public class GloberController {
	
	@RequestMapping(value="/getByName/{name}", method=RequestMethod.GET)
	public List<Glober> getByName(@PathVariable() String name) {	
		return new GloberDao().getGloberByName(name);
	}
	
	@RequestMapping(value="/getByEmail/{email}", method=RequestMethod.GET)
	public List<Glober> getByMail(@PathVariable() String email) {	
		return new GloberDao().getGloberByEmail(email);
	}
	
	@RequestMapping(value="/getByProject/{project}", method=RequestMethod.GET)
	public List<Glober> getByProject(@PathVariable() String project) {		
		return new GloberDao().getGloberByProject(project);
	}
	
	@RequestMapping(value="/getById/{id}", method=RequestMethod.GET)
	public Glober getByID(@PathVariable() String id) {		
		return new GloberDao().getGloberByID(id);
	}
	
}

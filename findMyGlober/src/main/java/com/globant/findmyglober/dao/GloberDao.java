package com.globant.findmyglober.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.globant.findmyglober.db.DataBaseManager;
import com.globant.findmyglober.entity.Glober;

public class GloberDao {	
	
	String sqlFindGlober = "SELECT 	glober.idGlober, glober.userName, glober.name, " +
			"		glober.lastName, glober.phone, glober.extension, " + 
			"		glober.mobile, glober.mail, seat.description location, " +
			"		role.name role, project.name projectName, client.name client " +
			"FROM findmyglober.glober, findmyglober.seat, findmyglober.role, " +
			"	 findmyglober.client, findmyglober.project, findmyglober.assignment " +
			"WHERE glober.idSeat = seat.idSeat " +
			"and glober.idRole = role.idRole " +
			"and assignment.idGlober = glober.idGlober " +
			"and assignment.idProject = project.idProject " +
			"and client.idClient = project.idClient " ;

	public List<Glober> getGloberByName(String name) {
		
		String sqlFindGloberByMail =  sqlFindGlober +
				   "and glober.name LIKE '%" + name + "%'";
		
		Glober glober;					
		List<Glober> listGlobers = null;
		DataBaseManager manager = new DataBaseManager();

		try {
			ResultSet resultSet = manager.executeSelect(sqlFindGloberByMail);
			listGlobers = new ArrayList<Glober>();
			while (resultSet.next()) {
				//glober = new Glober();				
				glober = createGlober(resultSet);
				listGlobers.add(glober);
				glober = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listGlobers;
	}
	
	public List<Glober> getGloberByEmail(String globerEmail) {

		String sqlFindGloberByMail =  sqlFindGlober +				
									" and glober.mail LIKE '%" + globerEmail + "%'";
		Glober glober;					
		List<Glober> listGlobers = null;
		DataBaseManager manager = new DataBaseManager();

		try {
			ResultSet resultSet = manager.executeSelect(sqlFindGloberByMail);
			listGlobers = new ArrayList<Glober>();
			while (resultSet.next()) {
				//glober = new Glober();				
				glober = createGlober(resultSet);
				listGlobers.add(glober);
				glober = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listGlobers;
	}
	
	public List<Glober> getGloberByProject(String project) {
		
		String sqlFindGloberByProject =  sqlFindGlober + 
									" and project.name LIKE '%" + project + "%'";								
		Glober glober;					
		List<Glober> listGlobers = null;
		DataBaseManager manager = new DataBaseManager();
		
		try {			
			ResultSet resultSet = manager.executeSelect(sqlFindGloberByProject);
			listGlobers = new ArrayList<Glober>();
			
			while (resultSet.next()) {				
				//glober = new Glober();				
				glober = createGlober(resultSet);
				listGlobers.add(glober);
				glober = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listGlobers;
	}	
	
	public Glober getGloberByID(String idGlober) {
			
		String sqlFindGloberById =  sqlFindGlober + 
				"and glober.idGlober= " + idGlober ;								
								
		Glober glober = new Glober();
		DataBaseManager manager = new DataBaseManager();
		try {
			
			ResultSet resultSet = manager.executeSelect(sqlFindGloberById);
			while (resultSet.next()) {
				glober = createGlober(resultSet);				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return glober;	
	}
	
	
	private Glober createGlober(ResultSet resultSet) throws SQLException {
		Glober glober = new Glober();
		
		glober.setGloberId(resultSet.getInt("idGlober"));
		glober.setUserName(resultSet.getString("userName"));
		glober.setName(resultSet.getString("name"));				
		glober.setLastName(resultSet.getString("LastName"));
		glober.setPhone(resultSet.getString("phone"));
		glober.setExtension(resultSet.getString("extension"));
		glober.setMobile(resultSet.getString("mobile"));
		glober.setMail(resultSet.getString("mail"));
		glober.setSeat(resultSet.getString("location"));
		glober.setRole(resultSet.getString("role"));				
		glober.setProjectName(resultSet.getString("projectName"));
		glober.setClientName(resultSet.getString("client"));
		
		return glober;
	}
	
}
